const isArrayIncludeSymbol = (array, char) => {
    for (let i = 0; i < array.length; i++) {
        if (char === array[i]) {
            return true;
        }
    }

    return false;
}

const charNumbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
const stringMaxLength = 100;
const isDelimiter = char => char === ' ';
const isOperator = char => char === '+' || char === '-' || char === '*' || char === '/';
const isUnaryOperator = char => char === '+' || char === '-';
const getMinusRPNUnaryOperator = () => '±';
const isRpnOperator = char => isOperator(char) || char === getMinusRPNUnaryOperator();
const isNumber = char => isArrayIncludeSymbol(charNumbers, char);
const charNumberToNumber = char => {
    for (let i = 0; i < charNumbers.length; i++) {
        if (char === charNumbers[i]) {
            return i;
        }
    }
}
const getOperationPriority = operator => {
    if (operator === '+' || operator === '-') {
        return 1;
    } else if (operator === '*' || operator === '/') {
        return 2;
    } else if (operator === getMinusRPNUnaryOperator()) {
        return 3;
    } else {
        return -1;
    }
};
const validateInputString = (string, stringMaxLength = 100) => {
    if (typeof string !== 'string') {
        return { isOk: false, error: 'expression required error' };
    }

    if (string.length > stringMaxLength) {
        return { isOk: false, error: 'max length error' };
    }

    for (let i = 0; i < string.length; i += 1) {
        if (!(isDelimiter(string[i]) === true || isNumber(string[i]) === true || isOperator(string[i]) === true || string[i] === ')' || string[i] === '(')) {
            return { isOk: false, error: `unsupported symbol: ${string[i]}` };
        }
    }
    return { isOk: true };
};

const validateRPNString = (string) => {
    for (let i = 0; i < string.length; i += 1) {
        if (!(isNumber(string[i]) === true || isRpnOperator(string[i]) === true)) {
            return { isOk: false, error: `unsupported symbol: ${string[i]}` };
        }
    }
    return { isOk: true };
}

const operators = {
    '+': (x, y) => x + y,
    '-': (x, y) => x - y,
    '*': (x, y) => x * y,
    '/': (x, y) => x / y,
    '±': x => -x,
};

const calculate = (string) => {
    stack = [];
    let result;
    string.split('').forEach((symbol, index) => {
        if (symbol in operators) {
            if (symbol === '±') {
                if (stack.length === 0) {
                    throw new Error('bad expression');
                }

                index === string.length - 1 ? result = operators[symbol](stack.pop()) : stack.push(operators[symbol](stack.pop()))
            } else {
                if (stack.length < 2) {
                    throw new Error('bad expression');
                }

                const [y, x] = [stack.pop(), stack.pop()];
                index === string.length - 1 ? result = operators[symbol](x, y) : stack.push(operators[symbol](x, y))
            }
        } else {
            stack.push(parseInt(symbol));
        }
    });

    return result;
}

const convertOpeningBracket = ({ symbol, stack, result, nextUnary }) => {
    stack.push(symbol);
    return { symbol, stack, result, nextUnary: true }
}

const convertClosingBracket = ({ symbol, stack, result, nextUnary }) => {
    if (stack.length === 0) {
        throw new Error('bad expression');
    }

    let el = stack.pop();
    while (el !== '(') {
        result = result + el;

        if (stack.length === 0) {
            throw new Error('bad expression')
        }

        el = stack.pop();
    }

    return { symbol, stack, result, nextUnary: false };
}

const convertOperator = ({ symbol, stack, result, nextUnary }) => {
    if (nextUnary && isUnaryOperator(symbol)) {
        if (symbol === '-') {
            if (stack.length > 0 && (getOperationPriority(stack[stack.length - 1]) >= getOperationPriority(getMinusRPNUnaryOperator()))) {
                result = result + stack.pop();
            }
            stack.push(getMinusRPNUnaryOperator());
        } else {

        }
    } else {
        let el;

        if (stack.length > 0) {
            el = stack[stack.length - 1];
        }

        if (getOperationPriority(el) >= getOperationPriority(symbol)) {
            result = result + stack.pop();
        }

        stack.push(symbol);
    }

    return { symbol, stack, result, nextUnary: true };
}

const convertNumber = ({ symbol, result, stack }) => {
    result = result + symbol;
    return { symbol, stack, result, nextUnary: false }
}

const toRPN = (string) => {
    let stack = [];
    let result = '';
    let nextUnary = true;

    for (let i = 0; i < string.length; i += 1) {
        const symbol = string[i];
        if (isDelimiter(symbol)) {
            continue;
        }
        if (symbol === '(') {
            const res = convertOpeningBracket({ symbol, stack, result, nextUnary });
            stack = res.stack;
            result = res.result;
            nextUnary = res.nextUnary;
        } else if (symbol === ')') {
            const res = convertClosingBracket({ symbol, stack, result, nextUnary });
            stack = res.stack;
            result = res.result;
            nextUnary = res.nextUnary;
        } else if (isOperator(symbol)) {
            const res = convertOperator({ symbol, stack, result, nextUnary });
            stack = res.stack;
            result = res.result;
            nextUnary = res.nextUnary;
        } else {
            const res = convertNumber({ symbol, stack, result, nextUnary });
            stack = res.stack;
            result = res.result;
            nextUnary = res.nextUnary;
        }

    }

    while (stack.length) {
        result = result + stack.pop();
    }


    return result;
}


const main = (expressionString) => {
    const { isOk, error } = validateInputString(expressionString, stringMaxLength);

    if (!isOk) {
        console.log(error);
    }

    const rpnString = toRPN(expressionString);

    console.log(rpnString);

    const { isOk: isRPNOk, error: isRPNError } = validateRPNString(rpnString);
    if (!isRPNOk) {
        console.log(isRPNError);
    }

    const result = calculate(rpnString);

    console.log(result)
};

main('1-1+(-1+1)-(-1+2/2)+(3*0)/1')