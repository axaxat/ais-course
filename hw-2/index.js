
class Char {
    constructor(symbol) {
        this._isDelimiter = symbol === ' ' ? true : false;
        this._symbol = symbol;
    }

    toString() {
        return this._symbol;
    }

    get isDelimiter() {
        return this._symbol === ' ' ? true : false;
    }

    processChar(stack, result) {
        throw new Error('Abstract method. Implement me');
    }
}

class NumberChar extends Char {
    static get NUMBERS() {
        return ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    }

    constructor(symbol) {
        super(symbol);

        if (!NumberChar.NUMBERS.includes(symbol)) {
            throw new Error('Char is not a number');
        }
    }

    processChar(stack, result) {
        result.push(this);
        return false
    }

    get value() {
        return NumberChar.NUMBERS.indexOf(this._symbol);
    }
}

class OpeningBracket extends Char {
    constructor(symbol = '(') {
        super(symbol);
    }

    processChar(stack, result) {
        stack.push(this);
        return true;
    }
}

class ClosingBracket extends Char {
    constructor(symbol = ')') {
        super(symbol);
    }

    processChar(stack, result) {
        if (stack.length === 0) {
            throw new Error('Unexpected end of stack');
        }

        let el = stack.pop();

        while (!(el instanceof OpeningBracket)) {
            result.push(el);

            if (stack.length === 0) {
                throw new Error('Unexpected end of stack');
            }

            el = stack.pop();
        }

        return false;
    }
}

class Operation extends Char {
    constructor(symbol) {
        super(symbol);
    }

    processChar(stack, result, nextUnary) {
        if (nextUnary && this instanceof Minus) {
            const char2 = new UnaryMinus();
            if (stack.length > 0 && stack[stack.length - 1] instanceof Operation && stack[stack.length - 1].priority >= char2.priority) {
                result.push(stack.pop());
            }
            stack.push(char2);
        } else {
            let el;

            if (stack.length > 0) {
                el = stack[stack.length - 1];
            }

            if (el && el instanceof Operation && el.priority >= this.priority) {
                result.push(stack.pop());
            }

            stack.push(this);
        }

        return true;
    }


    calculate(a, b) {
        throw new Error('Abstract method. Implement me');
    }
}

class Minus extends Operation {
    constructor(symbol = '-') {
        super(symbol);
    }

    priority() {
        return 1
    }
    calculate(a, b) {
        return a - b;
    }
}

class Plus extends Operation {
    constructor(symbol = '+') {
        super(symbol);
    }

    priority() {
        return 1
    }
    calculate(a, b) {
        return a + b;
    }
}

class Multiply extends Operation {
    constructor(symbol = '*') {
        super(symbol);
    }

    priority() {
        return 2
    }

    calculate(a, b) {
        return a * b;
    }
}

class Divide extends Operation {
    constructor(symbol = '/') {
        super(symbol);
    }

    priority() {
        return 2
    }

    calculate(a, b) {
        if (b === 0) {
            throw new Error('Divide by zero');
        }
        return a / b;
    }
}

class UnaryMinus extends Operation {
    constructor(symbol = '±') {
        super(symbol);
    }

    get isUnary() {
        return true;
    }

    priority() {
        return 3
    }
    calculate(a) {
        return -1 * a;
    }
}

class CharFabric {
    getCharInstance(symbol) {
        switch (true) {
            case symbol === '+': return new Plus();
            case symbol === '-': return new Minus();
            case symbol === '*': return new Multiply();
            case symbol === '/': return new Divide();
            case symbol === '(': return new OpeningBracket();
            case symbol === ')': return new ClosingBracket();
            case NumberChar.NUMBERS.includes(symbol): return new NumberChar(symbol);
            case [' '].includes(symbol): return new Char(symbol);
            default: throw new Error(`Bad symbol: ${symbol}`);
        }
    }
}

class Main {
    constructor(string) {
        this.charFabric = new CharFabric();

        this._string = string;
        this._validate(this._string);

        this.maxLength = 100;
    }

    _validate() {
        if (typeof this._string !== 'string') {
            throw new Error('Expression required');
        }

        if (this._string.length > this.maxLength) {
            throw new Error(`String max length: ${this.maxLength}`);
        }
    }

    calculate() {
        this._rpn = this._convertToRPN(this._string);
        this._result = this._calculateExpression(this._rpn);
    }

    getResult() {
        console.log(this._rpn.map(el => el.toString()).join(''));
        console.log(this._result);
    }


    _convertToRPN(string) {
        const stack = [];
        let result = [];
        let nextUnary = true;

        for (let i = 0; i < string.length; i += 1) {
            const char = this.charFabric.getCharInstance(string[i]);

            if (char.isDelimiter) {
                continue;
            }

            nextUnary = char.processChar(stack, result, nextUnary);
        }

        while (stack.length) {
            result.push(stack.pop());
        }

        return result;
    }

    _calculateExpression(rpn) {
        const stack = [];

        for (let i = 0; i < rpn.length; i += 1) {
            const el = rpn[i];
            if (el instanceof Operation) {
                let res;
                if (el.isUnary) {
                    if (stack.length === 0) {
                        throw new Error('Bad expression');
                    }

                    res = el.calculate(stack.pop());
                } else {
                    if (stack.length < 2) {
                        throw new Error('Bad expression');
                    }
                    const r = stack.pop();
                    const l = stack.pop();
                    res = el.calculate(l, r);
                }

                if (i === rpn.length - 1) {
                    return res;
                } else {
                    stack.push(res);
                }
            } else {
                if (el instanceof NumberChar) {
                    stack.push(el.value);
                } else {
                    throw new Error('Bad expression');
                }
            }
        }
    }
}



const inst = new Main('-1+1+1/1-1-1*(1+1/2)');
inst.calculate()
inst.getResult();


