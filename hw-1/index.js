const isArrayIncludeSymbol = (array, char) => {
    for (let i = 0; i < array.length; i++) {
        if (char === array[i]) {
            return true;
        }
    }

    return false;
}

const charNumbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
const stringMaxLength = 100;
const isDelimiter = char => char === ' ';
const isOperator = char => char === '+' || char === '-' || char === '*' || char === '/';
const isUnaryOperator = char => char === '+' || char === '-';
const getMinusRPNUnaryOperator = () => '±';
const isRpnOperator = char => isOperator(char) || char === getMinusRPNUnaryOperator();
const isNumber = char => isArrayIncludeSymbol(charNumbers, char);
const charNumberToNumber = char => {
    for (let i = 0; i < charNumbers.length; i++) {
        if (char === charNumbers[i]) {
            return i;
        }
    }
}

const getOperationPriority = operator => {
    if (operator === '+' || operator === '-') {
        return 1;
    } else if (operator === '*' || operator === '/') {
        return 2;
    } else if (operator === getMinusRPNUnaryOperator()) {
        return 3;
    } else {
        return -1;
    }
};


const main = (expressionString) => {
    console.log(expressionString);

    const toRPN = (string) => {
        const stack = [];
        let result = '';
        let nextUnary = true;

        for (let i = 0; i < string.length; i += 1) {
            if (!isDelimiter(string[i])) {
                if (string[i] === '(') {
                    stack.push(string[i]);
                    nextUnary = true;
                } else if (string[i] === ')') {

                    if (stack.length === 0) {
                        isFailed = true;
                        return
                    }

                    let el = stack.pop();
                    while (el !== '(') {
                        result = result + el;

                        if (stack.length === 0) {
                            isFailed = true;
                            return
                        }

                        el = stack.pop();
                    }
                    nextUnary = false;
                } else if (isOperator(string[i])) {
                    if (nextUnary && isUnaryOperator(string[i])) {
                        if (string[i] === '-') {
                            if (stack.length > 0 && (getOperationPriority(stack[stack.length - 1]) >= getOperationPriority(getMinusRPNUnaryOperator()))) {
                                result = result + stack.pop();
                            }
                            stack.push(getMinusRPNUnaryOperator());
                        } else {

                        }
                    } else {
                        let el;

                        if (stack.length > 0) {
                            el = stack[stack.length - 1];
                        }

                        if (getOperationPriority(el) >= getOperationPriority(string[i])) {
                            result = result + stack.pop();
                        }

                        stack.push(string[i]);
                    }

                    nextUnary = true;
                } else {
                    result = result + string[i];
                    nextUnary = false;
                }
            }
        }

        while (stack.length) {
            result = result + stack.pop();
        }


        return result;
    }

    const calculateOperation = (stack, operator) => {
        if (operator === getMinusRPNUnaryOperator()) {
            if (stack.length === 0) {
                isFailed = true;
                return
            }

            return -1 * stack.pop();
        } else {
            if (stack.length < 2) {
                isFailed = true;
                return
            }
            const r = stack.pop();
            const l = stack.pop();

            if (operator === '+') {
                return l + r;
            } else if (operator === '-') {
                return l - r;
            } else if (operator === '*') {
                return l * r;
            } else if (operator === '/') {
                if (r === 0) {
                    isFailed = true;
                    return;
                }

                return l / r;
            }
        }
    }

    const calculateExpression = (rpnString) => {
        const stack = [];

        for (let i = 0; i < rpnString.length; i += 1) {
            if (isRpnOperator(rpnString[i])) {
                let res = calculateOperation(stack, rpnString[i]);

                if (isFailed) {
                    return;
                }

                if (i === rpnString.length - 1) {
                    return res;
                } else {
                    stack.push(res);
                }
            } else {
                stack.push(charNumberToNumber(rpnString[i]));
            }
        }
    }

    if (typeof expressionString !== 'string') {
        console.log('expression required error');
        return;
    }

    if (expressionString.length > stringMaxLength) {
        console.log('max length error');
        return;
    }

    let isFailed = false;
    for (let i = 0; i < expressionString.length; i += 1) {
        if (!(isDelimiter(expressionString[i]) === true || isNumber(expressionString[i]) === true || isOperator(expressionString[i]) === true || expressionString[i] === ')' || expressionString[i] === '(')) {
            console.log('bad expression error');
            return;
        }
    }

    const rpnString = toRPN(expressionString);

    if (isFailed) {
        console.log('convert to rpn error');
        return;
    }

    console.log(rpnString);

    for (let i = 0; i < rpnString.length; i += 1) {
        if (!(isNumber(rpnString[i]) === true || isRpnOperator(rpnString[i]) === true)) {
            console.log('bad rpn error');
            return
        }
    }


    const result = calculateExpression(rpnString);

    if (isFailed) {
        console.log('calculate error');
        return;
    }

    console.log(result)
};



main('1-1+(-1+1)-(-1+2/2)+(3*0)/1')
